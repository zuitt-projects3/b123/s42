/*
  JS Dom (Document Object Model)
  In CSS, we had a concept of a Box Model. In JavaScript, we have Document Object Model. This allows us to manipulate our HTML elements with JavaScript.
  Because for JavaScript , all elements are considered as objects.

  In such a case, our elements as objects, we can access and manipulate the properties of an element.

*/

console.log(document);
// document refers to the whole page.
// .querySelector() - is a method that can be use to select a specific element from our document. The querySelector() uses CSS like selectors to select an element
let firstNameLabel = document.querySelector("#label-first-name");

// We were able to select an element by its id from our document and saved it in a variable.
console.log(firstNameLabel);

// .innerHTML is a property of an element which considers all the children of the selected element as a string. This includes other elements and text-content

console.log(firstNameLabel.innerHTML)

// Re-assigned the value of the innerHTML property of the firstNameLabel.
firstNameLabel.innerHTML = "I like New York City"

/*
  Mini-Activity
*/

let lastNameLabel = document.querySelector("#label-last-name")

lastNameLabel.innerHTML = "My Favorite Food is Pizza";

let city = "Tokyo";

// set a conditional wherein if the city variable is not New york, we will show the value otherwise

if(city === "New York"){
  firstNameLabel.innerHTML = `I Like New York City`
} else {
  firstNameLabel.innerHTML = `I dont Like New York, I Like ${city}`
}

// Events - allows us to add interactivity to our page. Wherein, we can have our user interact with a page and our page can perform a task.

/*
    Event Listeners
    - allow us to listen or detect an interaction between the user and the page. on the event that the user clicks, presses a key, hover a selected element or etc. we will perform a function.
*/
firstNameLabel.addEventListener('click',()=>{
  firstNameLabel.innerHTML = "Yamete Nii-Chan!"
  // elements have a property called style which is the style of an element. style in js is also an object with properties.
  firstNameLabel.style.color = "indianred";
  firstNameLabel.style.fontSize = "10vh";
})

/*
  Mini-Activity
*/
// lastNameLabel.style.color = "blue";
// lastNameLabel.style.fontSize = "5vh";

lastNameLabel.addEventListener('click',()=>{
  if(lastNameLabel.style.color = "blue"){
      lastNameLabel.style.color = "black";
      lastNameLabel.style.fontSize = "16px";
    } else {
      lastNameLabel.style.color = "blue";
      lastNameLabel.style.fontSize = "5vh";
    }

});

// Keyup is an event wherein we are able to perform a task when the user lets go of a key. Keyup is best used in input elements that require key inputs


let inputLastName = document.querySelector("#txt-last-name");
let inputFirstName = document.querySelector("#txt-first-name");
// .value - is a property of mostly inout elements which contains the current value in the input element
// initial value of your input element
// console.log() ran only the first time our page loaded and does not run again .
console.log(inputFirstName.value);

// select the h3 and save it in a variable
let fullNameDisplay = document.querySelector("#full-name-display")
// inputFirstName.addEventListener('keyup',()=> {
//   // console.log(inputFirstName.value)
//   fullNameDisplay.innerHTML = inputFirstName.value;
// })

// syntax: elemeent.addEventListener(<event>, <function>)

// you can create a named function that will run by event listeners
const showName = () => {
  console.log(inputFirstName.value)
  console.log(inputLastName.value)
  fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`
}
inputFirstName.addEventListener('keyup',showName);

inputLastName.addEventListener('keyup',showName)
