let inputFirstName = document.querySelector("#first-name");
let inputLastName = document.querySelector("#last-name");

let fullNameDisplay = document.querySelector("#full-display")
let buttonSubmit = document.querySelector("#btn-submit")
function displayName(){
  fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`
}
function reset(){
  inputFirstName.value = "";
  inputLastName.value = "";
  fullNameDisplay.innerHTML = "";
}

function submitName(){
  if(inputFirstName.value !== "" && inputLastName.value !== ""){
    alert(`Thank you for registering ${inputFirstName.value} ${inputLastName.value}!`)
    reset()
  } else {
    alert("Please Input First name and Last Name!")
  }
}
inputFirstName.addEventListener('keyup',displayName);
inputLastName.addEventListener('keyup',displayName);
buttonSubmit.addEventListener('click', submitName)
